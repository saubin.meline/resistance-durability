"""
/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\/
Resistance durability model for diploid individuals

Author: Méline Saubin, 2021, INRAE Nancy. ANR Clonix2D.

Versions:
Python-3.7.1
SimuPOP-1.1.9
argparse-1.1
/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\/
"""

import os.path
import simuPOP as sim
import random
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("ALLELE_VIR", help = "Initial frequency of virulent alleles in the population: favr", type=float)
parser.add_argument("K_S", help = "Carrying capacity on the S compartment", type=int)
parser.add_argument("K_R", help = "Carrying capacity on the R compartment", type=int)
parser.add_argument("r", help = "Growth rate of the pathogen population", type=float)
parser.add_argument("MIG", help = "Migration rate of the pathogen population between S and R compartments", type=float)
parser.add_argument("CYCLE_MELEZE", help = "Life cycle of the pathogen: \"avec_meleze\" for the life cycle with host alternation or \"sans_meleze\" without host alternation", type=str)
parser.add_argument("DOSSIER", help = "Name of the folder created in python to store the results", type=str)

args = parser.parse_args()

ALLELE_VIR = args.ALLELE_VIR
K_S = args.K_S
K_R = args.K_R
r = args.r
MIG = args.MIG
CYCLE_MELEZE = args.CYCLE_MELEZE
DOSSIER = args.DOSSIER

def main():
    '''
    Main function which models the evolution of the pathogen population for several
    replicates, and returns a table as a txt file with :
    1st row: the names of the main parameters
    2nd row: the values of the main parameters
    3rd row: A list of results obtained for each replicate
    '''
    # Model parameters:
    REP = 100               # Number of replicates
    GENERATIONS = 1100      # Number of generations
    C = 0.91                # Clonality rate
    SELECT_REPRO = 0.2      # Proportion of surviving individuals at each migration from S and R to A for the sexual reproduction.
    NB_ALL = 1              # Number of virulent avr alleles considered: 1 for C only, and 2 for C and O
    # Fitness of each genotypes on S and R. In this code, we consider one avirulent allele Avr (T) and two recessive virulent avr alleles (C and O):
    FITCC_S = 1
    FITCT_S = 1
    FITTT_S = 1
    FITCO_S = 1
    FITTO_S = 1
    FITOO_S = 1
    FITCC_R = 1
    FITCT_R = 0
    FITTT_R = 0
    FITCO_R = 1
    FITTO_R = 0
    FITOO_R = 1
    # Population initial sizes:
    if CYCLE_MELEZE=="avec_meleze":
        # With host alternation, the simulation starts on A
        N0_S = 0                # Initial size of the population on S (susceptible compartment)
        N0_R = 0                # Initial size of the population on R (resistant compartment)
        N0_M = 2000             # Initial size of the population on A (alternate host compartment, M in this code)
    elif CYCLE_MELEZE=="sans_meleze":
        # Without host alternation, the simulation starts on S
        N0_S = 2000             # Initial size of the population on S (susceptible compartment)
        N0_R = 0                # Initial size of the population on R (resistant compartment)

    DONNEES_INTERMEDIAIRES = "non" # If "oui", exportation of all intermediate data (restricted design), takes longer and requires a lot of memory.

    # Output name file
    NOM = DOSSIER+'.txt'

    if DONNEES_INTERMEDIAIRES=="non":
        # Directory of the folder, one unique folder for eavery result file
        DOSSIERDIR = "/home/msaubin/"
        os.mkdir(DOSSIERDIR)
        fichier_variation = os.path.join(DOSSIERDIR,NOM)

    if DONNEES_INTERMEDIAIRES=="oui":
        # one different directory for each result file
        os.mkdir(DOSSIER)
        fichier_variation = os.path.join(DOSSIER,NOM)

    resultats = []

    for i in range(REP):
        resultats.append(simulation(ALLELE_VIR, N0_S, N0_R, N0_M, K_S, K_R, r, GENERATIONS, MIG, C, FITCC_S, FITCT_S, FITTT_S, FITCO_S, FITTO_S, FITOO_S, FITCC_R, FITCT_R, FITTT_R, FITCO_R, FITTO_R, FITOO_R, SELECT_REPRO, CYCLE_MELEZE, NB_ALL, DOSSIER, i, DONNEES_INTERMEDIAIRES))

    with open(fichier_variation, 'w') as genepop_file_modif :
        NOMS = ["ALLELE_VIR", "K_S", "K_R", "r", "MIG", "CYCLE_MELEZE", "NB_ALL"]
        PARAMETRES = [ALLELE_VIR, K_S, K_R, r, MIG, CYCLE_MELEZE, NB_ALL]
        genepop_file_modif.write(' '.join([str(x) for x in NOMS]))
        genepop_file_modif.write('\n')
        genepop_file_modif.write(' '.join([str(x) for x in PARAMETRES]))
        genepop_file_modif.write('\n')
        genepop_file_modif.write(' '.join([str(x) for x in resultats]))
        genepop_file_modif.write('\n')

    return()

#--------------------Simulations---------------------#

def simulation(ALLELE_VIR, N0_S, N0_R, N0_M, K_S, K_R, r, GENERATIONS, MIG, C, FITCC_S, FITCT_S, FITTT_S, FITCO_S, FITTO_S, FITOO_S, FITCC_R, FITCT_R, FITTT_R, FITCO_R, FITTO_R, FITOO_R, SELECT_REPRO, CYCLE_MELEZE, NB_ALL, DOSSIER, REP, DONNEES_INTERMEDIAIRES):
    '''
    Function to model the evolution of one population of initial size N0_R,
    N0_S and N0_M on compartments R, S and A, respectively for GENERATIONS
    generations, and one replicate.
    Input:
        All parameters described in the main function, except the number of
        replicates.
    Output:
        List of results for the simulation, list of 4, 6, 7 or 10 elements:
        For the following explanations, "C" and "O" represent virulent alleles,
        "T" represents the avirulent allele, and
        4 elements if NB_ALL=1:
            [simulation equilibrium(fixation of "C", fixation of "T", "extinction", or "equilibre" for the coexistence between C and T),
            generation of fixation or extinction,
            generation of apparition of the first homozygous CC,
            generation of apparition of the first individual on R]
        6 elements if NB_ALL=2 and the virulent alleles (C or O) go extinct:
            [fixation of "T" or "extinction",
            generation of fixation or extinction,
            generation of apparition of the first homozygous CC,
            generation of apparition of the first homozygous OO,
            generation of apparition of the first heterozygous CO,
            generation of apparition of the first individual on R]
        7 elements if NB_ALL=2 and fixation of one virulent allele:
            [fixation of "C" or "O",
            generation of fixation,
            generation of apparition of the first homozygous CC,
            generation of apparition of the first homozygous OO,
            generation of apparition of the first heterozygous CO,
            generation of apparition of the first individual on R,
            generation of extinction of T]
        10 elements if NB_ALL=2 and no allele fixation:
            ["equilibre",
            final generation,
            generation of apparition of the first homozygous CC,
            generation of apparition of the first homozygous OO,
            generation of apparition of the first heterozygous CO,
            generation of apparition of the first individual on R,
            ggeneration of extinction of T,
            proportion of T alleles at the last generation,
            proportion of C alleles at the last generation,
            proportion of O alleles at the last generation]
    '''
    # Number of generations per cycle
    CYCLE = int(1/(1-C))

    ALPHA = r-1

    # Growth functions, requiering parameters ALPHA, K_S and K_R
    def logistique_avec_meleze (pop):
        '''
        Function to model a discrete logistic growth for populations on S and R,
        and a discrete exponential growth on A (for the life cycle with host
        alternation).
        Input: List of population sizes before the reproduction on S, R, and A
        Output: List of population sizes after the reproduction on S, R, and A
        '''
        taille = []
        if pop.subPopSize(0)>K_S:
            # If the population size on S is higher that the carrying capacity
            # on S, the new population size is equal to K_S
            taille.append(K_S)
        else :
            # New population size, with decimals
            N = pop.subPopSize(0)+(ALPHA*pop.subPopSize(0)*(1-pop.subPopSize(0)/K_S))

            # Random draw to round the decimal number to the inferior or superior integer
            Xalea = random.random()

            # If Xalea<decimal part of N, we round up to the superior integer,
            # and to the inferior integer otherwise
            if Xalea <= (N-int(N)):
                taille.append(int(N)+1)

            elif Xalea > (N-int(N)):
                taille.append(int(N))

            else :
                print("error in the logistic round number")

        if pop.subPopSize(1)>K_R:
            # If the population size on R is higher that the carrying capacity
            # on R, the new population size is equal to K_R
            taille.append(K_R)
        else :
            # New population size, with decimals
            M = pop.subPopSize(1)+(ALPHA*pop.subPopSize(1)*(1-pop.subPopSize(1)/K_R))

            # Random draw to round the decimal number to the inferior or superior integer
            Xalea = random.random()

            # If Xalea<decimal part of M, we round up to the superior integer,
            # and to the inferior integer otherwise
            if Xalea <= (M-int(M)):
                taille.append(int(M)+1)

            elif Xalea > (M-int(M)):
                taille.append(int(M))

            else :
                print("error in the logistic round number")

        # New population size on A, with decimals
        O = r*pop.subPopSize(2)
        # Random draw to round the decimal number to the inferior or superior integer
        Xalea = random.random()

        # If Xalea<decimal part of O, we round up to the superior integer,
        # and to the inferior integer otherwise
        if Xalea <= (O-int(O)):
            taille.append(int(O)+1)

        elif Xalea > (O-int(O)):
            taille.append(int(O))
        else :
            print("error in the logistic round number")

        return (taille)

    def logistique_sans_meleze (pop):
        '''
        Function to model a discrete logistic growth for populations on S and R
        (for the life cycle without host alternation).
        Input: List of population sizes before the reproduction on S and  R
        Output: List of population sizes after the reproduction on S and  R
        '''
        taille = []
        if pop.subPopSize(0)>K_S:
            # If the population size on S is higher that the carrying capacity
            # on S, the new population size is equal to K_S
            taille.append(K_S)
        else :
            # New population size, with decimals
            N = pop.subPopSize(0)+(ALPHA*pop.subPopSize(0)*(1-pop.subPopSize(0)/K_S))

            # Random draw to round the decimal number to the inferior or superior integer
            Xalea = random.random()

            # If Xalea<decimal part of N, we round up to the superior integer,
            # and to the inferior integer otherwise
            if Xalea <= (N-int(N)):
                taille.append(int(N)+1)

            elif Xalea > (N-int(N)):
                taille.append(int(N))

            else :
                print("error in the logistic round number")

        if pop.subPopSize(1)>K_R:
            taille.append(K_R)
        else :
            # New population size, with decimals
            M = pop.subPopSize(1)+(ALPHA*pop.subPopSize(1)*(1-pop.subPopSize(1)/K_R))

            # Random draw to round the decimal number to the inferior or superior integer
            Xalea = random.random()

            # If Xalea<decimal part of M, we round up to the superior integer,
            # and to the inferior integer otherwise
            if Xalea <= (M-int(M)):
                taille.append(int(M)+1)

            elif Xalea > (M-int(M)):
                taille.append(int(M))

            else :
                print("error in the logistic round number")
        return (taille)

    if CYCLE_MELEZE == "avec_meleze" :
        # For the life cycle with host alternation

        if NB_ALL == 1 :
            # For one virulent allele only

            # Initialisation of the population, diploid, with 3 subpopulations (S, R and A)
            pop = sim.Population(size=[N0_S, N0_R, N0_M], ploidy = 2, loci=[1], infoFields=['migrate_to'])

            # Evolution of the population for gen generations
            pop.evolve(
                   # Initialisation of the frequencies of the virulent allele at the beginning
                   # The first allele is "T", the second "C"
                   initOps = [sim.InitGenotype(prop = [1-ALLELE_VIR, ALLELE_VIR], loci = [0], subPops = [2]),
                              # Initialisation of empty lists of parameters to monitor through the evolution process:
                              # Population sizes
                              sim.PyExec('popS_taille=[]'),
                              sim.PyExec('popR_taille=[]'),
                              sim.PyExec('popM_taille=[]'),
                              sim.PyExec('popS_taille_avant_sexe=[]'),
                              sim.PyExec('popR_taille_avant_sexe=[]'),
                              sim.PyExec('popM_taille_avant_sexe=[]'),
                              # Number of CC homozygous
                              sim.PyExec('HomozygoteCC=[]'),
                              # Allele frequencies
                              sim.PyExec('trajC=[]'),
                              sim.PyExec('trajT=[]')],
                   preOps = [# Migration in proportion of the population, with a matrix of migration:
                             # m rows, n columns, with m the initial population, and n the population of arrival.
                             # Once per cycle, every individual migrate on the A compartment for the sexual reproduction
                             sim.Migrator(rate = [[0,0,1], [0,0,1],[0,0,0]], mode=sim.BY_PROBABILITY, begin = CYCLE, step = CYCLE),
                             # Survival of only a proportion of individuals at their arrival on A
                             sim.MaPenetrance(loci=0, penetrance=[1-SELECT_REPRO, 1-SELECT_REPRO, 1-SELECT_REPRO], subPops = [2], begin = CYCLE, step = CYCLE),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Migration between S and R at every generation
                             sim.Migrator(rate = [[0,MIG,0], [MIG,0,0],[0,0,0]], mode=sim.BY_PROBABILITY),
                             # Death of individuals on S and R depending on the fitness value of their genotypes
                             sim.MaPenetrance(loci=0, penetrance=[1-FITTT_S, 1-FITCT_S, 1-FITCC_S], subPops = [0]),
                             sim.MaPenetrance(loci=0, penetrance=[1-FITTT_R, 1-FITCT_R, 1-FITCC_R], subPops = [1]),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Implementation of data lists
                             sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                             sim.PyExec('popS_taille_avant_sexe.append(subPopSize[0])'),
                             sim.PyExec('popR_taille_avant_sexe.append(subPopSize[1])'),
                             sim.PyExec('popM_taille_avant_sexe.append(subPopSize[2])')],
                   # Alternation of two types of reproduction:
                   # Sexual reproduction, hermaphrodite with selfing, once per cycle on A, followed by an asexual reproduction
                   # Asexual reproduction on S and R otherwise
                   matingScheme = sim.ConditionalMating(
                           condition(CYCLE),
                           sim.HermaphroditicMating(allowSelfing = True),
                           sim.RandomSelection(subPopSize = logistique_avec_meleze)),
                   postOps=[# Migration after the reproduction on A: individuals are sorted between S and R, depending on K_S and K_R
                            sim.Migrator(rate = [[0,0,0], [0,0,0],[K_S/(K_S+K_R),K_R/(K_S+K_R),0]], mode=sim.BY_PROPORTION, begin = 1, step = CYCLE),
                            # Death of individuals on S and R depending on the fitness value of their genotypes
                            sim.MaPenetrance(loci=0, penetrance=[1-FITTT_S, 1-FITCT_S, 1-FITCC_S], subPops = [0]),
                            sim.MaPenetrance(loci=0, penetrance=[1-FITTT_R, 1-FITCT_R, 1-FITCC_R], subPops = [1]),
                            sim.MaPenetrance(loci=0, penetrance=[1, 1, 1], subPops = [2], begin = 1, step = CYCLE),
                            sim.DiscardIf('ind.affected()', exposeInd='ind'),
                            # Implementation of data lists
                            sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                            sim.PyExec('popS_taille.append(subPopSize[0])'),
                            sim.PyExec('popR_taille.append(subPopSize[1])'),
                            sim.PyExec('popM_taille.append(subPopSize[2])'),
                            sim.PyExec('HomozygoteCC.append(genoFreq[0][(1, 1)])'),
                            sim.PyExec('trajC.append(alleleFreq[0][1])'),
                            sim.PyExec('trajT.append(alleleFreq[0][0])'),
                            # End of the simulation forced in case of fixation of C or T
                            sim.TerminateIf('(alleleFreq[0][1] == 1) and (subPopSize[0]>7000) and (gen>1100)'),
                            sim.TerminateIf('(alleleFreq[0][0] == 1) and (subPopSize[0]>7000) and (gen>1100)')],
                   # Total number of generations
                   gen = GENERATIONS)

            if DONNEES_INTERMEDIAIRES == "oui" :
                # Exportation of all the intermediate data, with the evolution of population sizes and allelic frequencies
                NOMS = ["NB_INDIV_S", "NB_INDIV_R", "NB_INDIV_S_AVANT_SEXE", "NB_INDIV_R_AVANT_SEXE", "PROP_C_S", "PROP_T_S"]
                donnees = [pop.dvars().popS_taille, pop.dvars().popR_taille, pop.dvars().popS_taille_avant_sexe, pop.dvars().popR_taille_avant_sexe, pop.dvars().trajC, pop.dvars().trajT]
                nom_donnees = "donnees_intermediaires_rep%d.txt" % (REP)
                fichier_donnees = os.path.join(DOSSIER, nom_donnees)

                with open(fichier_donnees, 'w') as file_data :
                    file_data.write(' '.join([str(x) for x in NOMS]))
                    file_data.write('\n')
                    file_data.write(' '.join([str(x) for x in donnees]))
                    file_data.write('\n')

            # Generation of first observation of an homozygous CC
            hCC = premiere_apparition(pop.dvars().HomozygoteCC)

            # Generation of first observation of an individual on R
            R = premiere_apparition(pop.dvars().popR_taille)

            if ((pop.dvars().popS_taille[-1]+pop.dvars().popR_taille[-1]+pop.dvars().popM_taille[-1]) == 0):
                # If the population is extinct at the last generation, result = "extinction"
                for i in range(len(pop.dvars().popS_taille)):
                    if ((pop.dvars().popS_taille[i]+pop.dvars().popR_taille[i]+pop.dvars().popM_taille[i]) == 0):
                        return(["extinction", i, hCC, R])
            else :
                for j in range(len(pop.dvars().trajC)):
                    # If there is a fixation of the virulent allele
                    if (pop.dvars().trajC[j] == 1):
                        return(["C",j, hCC, R])
                    # If there is a fixation of the avirulent allele
                    elif (pop.dvars().trajT[j] == 1):
                        return(["T",j, hCC, R])

            # If there is neither fixation nor extinction
            return(["equilibre",GENERATIONS, hCC, R])

        elif NB_ALL == 2 :
            # For two distinct virulent alleles

            # Initialisation of the population, diploid, with 3 subpopulations (S, R and A)
            pop = sim.Population(size=[N0_S, N0_R, N0_M], ploidy = 2, loci=[1], infoFields=['migrate_to'])

            # Evolution of the population for gen generations
            pop.evolve(
                   # Initialisation of the frequencies of the virulent allele at the beginning
                   # The first allele is "T", the second "C", the third "O"
                   initOps = [sim.InitGenotype(prop = [1-2*ALLELE_VIR, ALLELE_VIR, ALLELE_VIR], loci = [0], subPops = [2]),
                              # Initialisation of empty lists of parameters to monitor through the evolution process:
                              # Population sizes
                              sim.PyExec('popS_taille=[]'),
                              sim.PyExec('popR_taille=[]'),
                              sim.PyExec('popM_taille=[]'),
                              sim.PyExec('popS_taille_avant_sexe=[]'),
                              sim.PyExec('popR_taille_avant_sexe=[]'),
                              sim.PyExec('popM_taille_avant_sexe=[]'),
                              # Number of genotypes
                              sim.PyExec('HomozygoteCC=[]'),
                              sim.PyExec('HomozygoteOO=[]'),
                              sim.PyExec('HeterozygoteCO=[]'),
                              # Allele frequencies
                              sim.PyExec('trajC=[]'),
                              sim.PyExec('trajO=[]'),
                              sim.PyExec('trajT=[]')],
                   preOps = [# Migration in proportion of the population, with a matrix of migration:
                             # m rows, n columns, with m the initial population, and n the population of arrival.
                             # Once per cycle, every individual migrate on the A compartment for the sexual reproduction
                             sim.Migrator(rate = [[0,0,1], [0,0,1],[0,0,0]], mode=sim.BY_PROBABILITY, begin = CYCLE, step = CYCLE),
                             # Survival of only a proportion of individuals at their arrival on A
                             sim.MapPenetrance(loci=0, penetrance={(0,0): 1-SELECT_REPRO, (0,1): 1-SELECT_REPRO, (1,1): 1-SELECT_REPRO, (0,2): 1-SELECT_REPRO, (1,2): 1-SELECT_REPRO, (2,2): 1-SELECT_REPRO}, subPops = [2], begin = CYCLE, step = CYCLE),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Migration between S and R at every generation
                             sim.Migrator(rate = [[0,MIG,0], [MIG,0,0],[0,0,0]], mode=sim.BY_PROBABILITY),
                             # Death of individuals on S and R depending on the fitness value of their genotypes
                             sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_S, (0,1): 1-FITCT_S, (1,1): 1-FITCC_S, (0,2): 1-FITTO_S, (1,2): 1-FITCO_S, (2,2): 1-FITOO_S}, subPops = [0]),
                             sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_R, (0,1): 1-FITCT_R, (1,1): 1-FITCC_R, (0,2): 1-FITTO_R, (1,2): 1-FITCO_R, (2,2): 1-FITOO_R}, subPops = [1]),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Implementation of data lists
                             sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                             sim.PyExec('popS_taille_avant_sexe.append(subPopSize[0])'),
                             sim.PyExec('popR_taille_avant_sexe.append(subPopSize[1])'),
                             sim.PyExec('popM_taille_avant_sexe.append(subPopSize[2])')],
                   # Alternation of two types of reproduction:
                   # Sexual reproduction, hermaphrodite with selfing, once per cycle on A, followed by an asexual reproduction
                   # Asexual reproduction on S and R otherwise
                   matingScheme = sim.ConditionalMating(
                           condition(CYCLE),
                           sim.HermaphroditicMating(allowSelfing = True),
                           sim.RandomSelection(subPopSize = logistique_avec_meleze)),
                   postOps=[# Migration after the reproduction on A: individuals are sorted between S and R, depending on K_S and K_R
                            sim.Migrator(rate = [[0,0,0], [0,0,0],[K_S/(K_S+K_R),K_R/(K_S+K_R),0]], mode=sim.BY_PROPORTION, begin = 1, step = CYCLE),
                            # Death of individuals on S and R depending on the fitness value of their genotypes
                            sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_S, (0,1): 1-FITCT_S, (1,1): 1-FITCC_S, (0,2): 1-FITTO_S, (1,2): 1-FITCO_S, (2,2): 1-FITOO_S}, subPops = [0]),
                            sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_R, (0,1): 1-FITCT_R, (1,1): 1-FITCC_R, (0,2): 1-FITTO_R, (1,2): 1-FITCO_R, (2,2): 1-FITOO_R}, subPops = [1]),
                            sim.MapPenetrance(loci=0, penetrance= {(0,0): 1, (0,1): 1, (1,1): 1, (0,2): 1, (1,2): 1, (2,2): 1}, subPops = [2], begin = 1, step = CYCLE),
                            sim.DiscardIf('ind.affected()', exposeInd='ind'),
                            # Implementation of data lists
                            sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                            sim.PyExec('popS_taille.append(subPopSize[0])'),
                            sim.PyExec('popR_taille.append(subPopSize[1])'),
                            sim.PyExec('popM_taille.append(subPopSize[2])'),
                            sim.PyExec('HomozygoteCC.append(genoFreq[0][(1, 1)])'),
                            sim.PyExec('HomozygoteOO.append(genoFreq[0][(2, 2)])'),
                            sim.PyExec('HeterozygoteCO.append((genoFreq[0][(1, 2)]) + (genoFreq[0][(2, 1)]))'),
                            sim.PyExec('trajC.append(alleleFreq[0][1])'),
                            sim.PyExec('trajO.append(alleleFreq[0][2])'),
                            sim.PyExec('trajT.append(alleleFreq[0][0])'),
                            # End of the simulation forced in case of fixation of C, T or O
                            sim.TerminateIf('(alleleFreq[0][1] == 1) and (subPopSize[0]>7000) and (gen>110)'),
                            sim.TerminateIf('(alleleFreq[0][2] == 1) and (subPopSize[0]>7000) and (gen>110)'),
                            sim.TerminateIf('(alleleFreq[0][0] == 1) and (subPopSize[0]>7000) and (gen>110)')],
                   # Total number of generations
                   gen = GENERATIONS)

            if DONNEES_INTERMEDIAIRES == "oui" :
                # Exportation of all the intermediate data, with the evolution of population sizes and allelic frequencies
                NOMS = ["NB_INDIV_S", "NB_INDIV_R", "NB_INDIV_S_AVANT_SEXE", "NB_INDIV_R_AVANT_SEXE", "PROP_C_S", "PROP_T_S", "PROP_O_S"]
                donnees = [pop.dvars().popS_taille, pop.dvars().popR_taille, pop.dvars().popS_taille_avant_sexe, pop.dvars().popR_taille_avant_sexe, pop.dvars().trajC, pop.dvars().trajT, pop.dvars().trajO]
                nom_donnees = "donnees_intermediaires_rep%d.txt" % (REP)
                fichier_donnees = os.path.join(DOSSIER, nom_donnees)

                with open(fichier_donnees, 'w') as file_data :
                    file_data.write(' '.join([str(x) for x in NOMS]))
                    file_data.write('\n')
                    file_data.write(' '.join([str(x) for x in donnees]))
                    file_data.write('\n')

            # Generation of first observations of some genotypes
            hCC = premiere_apparition(pop.dvars().HomozygoteCC)
            hOO = premiere_apparition(pop.dvars().HomozygoteOO)
            hCO = premiere_apparition(pop.dvars().HeterozygoteCO)

            # Generation of first observation of an individual on R
            R = premiere_apparition(pop.dvars().popR_taille)

            # Generation of virulence fixation
            gen_virulence = fixation_virulence(pop.dvars().trajT)

            if ((pop.dvars().popS_taille[-1]+pop.dvars().popR_taille[-1]+pop.dvars().popM_taille[-1]) == 0):
                # If the population is extinct at the last generation, result = "extinction"
                for i in range(len(pop.dvars().popS_taille)):
                    if ((pop.dvars().popS_taille[i]+pop.dvars().popR_taille[i]+pop.dvars().popM_taille[i]) == 0):
                        return(["extinction", i, hCC, hOO, hCO, R])
            else :
                for j in range(len(pop.dvars().trajC)):
                    # If there is a fixation of the C allele
                    if (pop.dvars().trajC[j] == 1):
                        return(["C",j, hCC, hOO, hCO, R, gen_virulence])
                    # If there is a fixation of the O allele
                    if (pop.dvars().trajO[j] == 1):
                        return(["O",j, hCC, hOO, hCO, R, gen_virulence])
                    # If there is a fixation of the T allele
                    elif (pop.dvars().trajT[j] == 1):
                        return(["T",j, hCC, hOO, hCO, R])

            # If there is neither fixation nor extinction
            return(["equilibre",GENERATIONS, hCC, hOO, hCO, R, gen_virulence, pop.dvars().trajT[-1], pop.dvars().trajC[-1], pop.dvars().trajO[-1]])

        else :
            print("The number of alleles NB_ALL does not correspond to the expected values (1 or 2)")

    elif CYCLE_MELEZE == "sans_meleze":
        # For the life cycle without host alternation

        if NB_ALL == 1 :
            # For one virulent allele only

            # Initialisation of the population, diploid, with 2 subpopulations (S and R )
            pop = sim.Population(size=[N0_S, N0_R], ploidy = 2, loci=[1], infoFields=['migrate_to'])

            # Evolution of the population for gen generations
            pop.evolve(
                   # Initialisation of the frequencies of the virulent allele at the beginning
                   # The first allele is "T", the second "C"
                   initOps = [sim.InitGenotype(prop = [1-ALLELE_VIR, ALLELE_VIR], loci = [0], subPops = [0]),
                              # Initialisation of empty lists of parameters to monitor through the evolution process:
                              # Population sizes
                              sim.PyExec('popS_taille=[]'),
                              sim.PyExec('popR_taille=[]'),
                              sim.PyExec('popS_taille_avant_sexe=[]'),
                              sim.PyExec('popR_taille_avant_sexe=[]'),
                              # Number of CC homozygous
                              sim.PyExec('HomozygoteCC=[]'),
                              # Allele frequencies
                              sim.PyExec('trajC=[]'),
                              sim.PyExec('trajT=[]')],
                   preOps = [# Survival of only a proportion of individuals before the sexual reproduction
                             sim.MaPenetrance(loci=0, penetrance=[1-SELECT_REPRO, 1-SELECT_REPRO, 1-SELECT_REPRO], subPops = [0], begin = CYCLE, step = CYCLE),
                             sim.MaPenetrance(loci=0, penetrance=[1-SELECT_REPRO, 1-SELECT_REPRO, 1-SELECT_REPRO], subPops = [1], begin = CYCLE, step = CYCLE),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Migration between S and R at every generation
                             sim.Migrator(rate = [[0,MIG], [MIG,0]], mode=sim.BY_PROBABILITY),
                             # Death of individuals on S and R depending on the fitness value of their genotypes
                             sim.MaPenetrance(loci=0, penetrance=[1-FITTT_S, 1-FITCT_S, 1-FITCC_S], subPops = [0]),
                             sim.MaPenetrance(loci=0, penetrance=[1-FITTT_R, 1-FITCT_R, 1-FITCC_R], subPops = [1]),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Implementation of data lists
                             sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                             sim.PyExec('popS_taille_avant_sexe.append(subPopSize[0])'),
                             sim.PyExec('popR_taille_avant_sexe.append(subPopSize[1])')],
                   # Alternation of two types of reproduction on S and R:
                   # Sexual reproduction, hermaphrodite with selfing, once per cycle
                   # Asexual reproduction otherwise
                   matingScheme = sim.ConditionalMating(
                           condition(CYCLE),
                           sim.HermaphroditicMating(allowSelfing = True),
                           sim.RandomSelection(subPopSize = logistique_sans_meleze)),
                   postOps=[# Death of individuals on S and R depending on the fitness value of their genotypes
                            sim.MaPenetrance(loci=0, penetrance=[1-FITTT_S, 1-FITCT_S, 1-FITCC_S], subPops = [0]),
                            sim.MaPenetrance(loci=0, penetrance=[1-FITTT_R, 1-FITCT_R, 1-FITCC_R], subPops = [1]),
                            sim.DiscardIf('ind.affected()', exposeInd='ind'),
                            # Implementation of data lists
                            sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                            sim.PyExec('popS_taille.append(subPopSize[0])'),
                            sim.PyExec('popR_taille.append(subPopSize[1])'),
                            sim.PyExec('HomozygoteCC.append(genoFreq[0][(1, 1)])'),
                            sim.PyExec('trajC.append(alleleFreq[0][1])'),
                            sim.PyExec('trajT.append(alleleFreq[0][0])'),
                            # End of the simulation forced in case of fixation of C or T
                            sim.TerminateIf('(alleleFreq[0][1] == 1) and (subPopSize[0]>7000) and (gen>1100)'),
                            sim.TerminateIf('(alleleFreq[0][0] == 1) and (subPopSize[0]>7000) and (gen>1100)')],
                   # Total number of generations
                   gen = GENERATIONS)

            if DONNEES_INTERMEDIAIRES == "oui" :
                # Exportation of all the intermediate data, with the evolution of population sizes and allelic frequencies
                NOMS = ["NB_INDIV_S", "NB_INDIV_R", "NB_INDIV_S_AVANT_SEXE", "NB_INDIV_R_AVANT_SEXE", "PROP_C_S", "PROP_T_S"]
                donnees = [pop.dvars().popS_taille, pop.dvars().popR_taille, pop.dvars().popS_taille_avant_sexe, pop.dvars().popR_taille_avant_sexe, pop.dvars().trajC, pop.dvars().trajT]
                nom_donnees = "donnees_intermediaires_rep%d.txt" % (REP)
                fichier_donnees = os.path.join(DOSSIER, nom_donnees)

                with open(fichier_donnees, 'w') as file_data :
                    file_data.write(' '.join([str(x) for x in NOMS]))
                    file_data.write('\n')
                    file_data.write(' '.join([str(x) for x in donnees]))
                    file_data.write('\n')

            # Generation of first observation of an homozygous CC
            hCC = premiere_apparition(pop.dvars().HomozygoteCC)

            # Generation of first observation of an individual on R
            R = premiere_apparition(pop.dvars().popR_taille)

            if ((pop.dvars().popS_taille[-1]+pop.dvars().popR_taille[-1]) == 0):
                # If the population is extinct at the last generation, result = "extinction"
                for i in range(len(pop.dvars().popS_taille)):
                    if ((pop.dvars().popS_taille[i]+pop.dvars().popR_taille[i]) == 0):
                        return(["extinction", i, hCC, R])
            else :
                for j in range(len(pop.dvars().trajC)):
                    # If there is a fixation of the virulent allele
                    if (pop.dvars().trajC[j] == 1):
                        return(["C",j, hCC, R])
                    # If there is a fixation of the avirulent allele
                    elif (pop.dvars().trajT[j] == 1):
                        return(["T",j, hCC, R])

            # If there is neither fixation nor extinction
            return(["equilibre",GENERATIONS, hCC, R])

        elif NB_ALL == 2 :
            # For two distinct virulent alleles

            # Initialisation of the population, diploid, with 2 subpopulations (S and R)
            pop = sim.Population(size=[N0_S, N0_R], ploidy = 2, loci=[1], infoFields=['migrate_to'])

            # Evolution of the population for gen generations
            pop.evolve(
                   # Initialisation of the frequencies of the virulent allele at the beginning
                   # The first allele is "T", the second "C", the third "O"
                   initOps = [sim.InitGenotype(freq = [1-2*ALLELE_VIR, ALLELE_VIR, ALLELE_VIR], loci = [0], subPops = [0]),
                              # Initialisation of empty lists of parameters to monitor through the evolution process:
                              # Population sizes
                              sim.PyExec('popS_taille=[]'),
                              sim.PyExec('popR_taille=[]'),
                              sim.PyExec('popS_taille_avant_sexe=[]'),
                              sim.PyExec('popR_taille_avant_sexe=[]'),
                              # Number of genotypes
                              sim.PyExec('HomozygoteCC=[]'),
                              sim.PyExec('HomozygoteOO=[]'),
                              sim.PyExec('HeterozygoteCO=[]'),
                              # Allele frequencies
                              sim.PyExec('trajC=[]'),
                              sim.PyExec('trajO=[]'),
                              sim.PyExec('trajT=[]')],
                   preOps = [# Survival of only a proportion of individuals before the sexual reproduction
                             sim.MaPenetrance(loci=0, penetrance=[1-SELECT_REPRO, 1-SELECT_REPRO, 1-SELECT_REPRO], subPops = [0], begin = CYCLE, step = CYCLE),
                             sim.MaPenetrance(loci=0, penetrance=[1-SELECT_REPRO, 1-SELECT_REPRO, 1-SELECT_REPRO], subPops = [1], begin = CYCLE, step = CYCLE),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Migration between S and R at every generation
                             sim.Migrator(rate = [[0,MIG], [MIG,0]], mode=sim.BY_PROBABILITY),
                             # Death of individuals on S and R depending on the fitness value of there genotypes
                             sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_S, (0,1): 1-FITCT_S, (1,1): 1-FITCC_S, (0,2): 1-FITTO_S, (1,2): 1-FITCO_S, (2,2): 1-FITOO_S}, subPops = [0]),
                             sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_R, (0,1): 1-FITCT_R, (1,1): 1-FITCC_R, (0,2): 1-FITTO_R, (1,2): 1-FITCO_R, (2,2): 1-FITOO_R}, subPops = [1]),
                             sim.DiscardIf('ind.affected()', exposeInd='ind'),
                             # Implementation of data lists
                             sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                             sim.PyExec('popS_taille_avant_sexe.append(subPopSize[0])'),
                             sim.PyExec('popR_taille_avant_sexe.append(subPopSize[1])')],
                   # Alternation of two types of reproduction on S and R:
                   # Sexual reproduction, hermaphrodite with selfing, once per cycle
                   # Asexual reproduction otherwise
                   matingScheme = sim.ConditionalMating(
                           condition(CYCLE),
                           sim.HermaphroditicMating(allowSelfing = True),
                           sim.RandomSelection(subPopSize = logistique_sans_meleze)),
                   postOps=[# Death of individuals on S and R depending on the fitness value of their genotypes
                            sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_S, (0,1): 1-FITCT_S, (1,1): 1-FITCC_S, (0,2): 1-FITTO_S, (1,2): 1-FITCO_S, (2,2): 1-FITOO_S}, subPops = [0]),
                            sim.MapPenetrance(loci=0, penetrance={(0,0): 1-FITTT_R, (0,1): 1-FITCT_R, (1,1): 1-FITCC_R, (0,2): 1-FITTO_R, (1,2): 1-FITCO_R, (2,2): 1-FITOO_R}, subPops = [1]),
                            sim.DiscardIf('ind.affected()', exposeInd='ind'),
                            # Implementation of data lists
                            sim.Stat(popSize = True, alleleFreq=[0], genoFreq=0, vars = ['alleleFreq', 'alleleFreq_sp']),
                            sim.PyExec('popS_taille.append(subPopSize[0])'),
                            sim.PyExec('popR_taille.append(subPopSize[1])'),
                            sim.PyExec('HomozygoteCC.append(genoFreq[0][(1, 1)])'),
                            sim.PyExec('HomozygoteOO.append(genoFreq[0][(2, 2)])'),
                            sim.PyExec('HeterozygoteCO.append((genoFreq[0][(1, 2)]) + (genoFreq[0][(2, 1)]))'),
                            sim.PyExec('trajC.append(alleleFreq[0][1])'),
                            sim.PyExec('trajO.append(alleleFreq[0][2])'),
                            sim.PyExec('trajT.append(alleleFreq[0][0])'),
                            # End of the simulation forced in case of fixation of C, T or O
                            sim.TerminateIf('(alleleFreq[0][1] == 1) and (subPopSize[0]>7000) and (gen>110)'),
                            sim.TerminateIf('(alleleFreq[0][2] == 1) and (subPopSize[0]>7000) and (gen>110)'),
                            sim.TerminateIf('(alleleFreq[0][0] == 1) and (subPopSize[0]>7000) and (gen>110)')],
                   # Total number of generations
                   gen = GENERATIONS)

            if DONNEES_INTERMEDIAIRES == "oui" :
                # Exportation of all the intermediate data, with the evolution of population sizes and allelic frequencies
                NOMS = ["NB_INDIV_S", "NB_INDIV_R", "NB_INDIV_S_AVANT_SEXE", "NB_INDIV_R_AVANT_SEXE", "PROP_C_S", "PROP_T_S", "PROP_O_S"]
                donnees = [pop.dvars().popS_taille, pop.dvars().popR_taille, pop.dvars().popS_taille_avant_sexe, pop.dvars().popR_taille_avant_sexe, pop.dvars().trajC, pop.dvars().trajT, pop.dvars().trajO]
                nom_donnees = "donnees_intermediaires_rep%d.txt" % (REP)
                fichier_donnees = os.path.join(DOSSIER, nom_donnees)

                with open(fichier_donnees, 'w') as file_data :
                    file_data.write(' '.join([str(x) for x in NOMS]))
                    file_data.write('\n')
                    file_data.write(' '.join([str(x) for x in donnees]))
                    file_data.write('\n')

            # Generation of first observations of some genotypes
            hCC = premiere_apparition(pop.dvars().HomozygoteCC)
            hOO = premiere_apparition(pop.dvars().HomozygoteOO)
            hCO = premiere_apparition(pop.dvars().HeterozygoteCO)

            # Generation of first observation of an individual on R
            R = premiere_apparition(pop.dvars().popR_taille)

            # Generation of virulence fixation
            gen_virulence = fixation_virulence(pop.dvars().trajT)

            if ((pop.dvars().popS_taille[-1]+pop.dvars().popR_taille[-1]) == 0):
                # If the population is extinct at the last generation, result = "extinction"
                for i in range(len(pop.dvars().popS_taille)):
                    if ((pop.dvars().popS_taille[i]+pop.dvars().popR_taille[i]) == 0):
                        return(["extinction", i, hCC, hOO, hCO, R])
            else :
                for j in range(len(pop.dvars().trajC)):
                    # If there is a fixation of the C allele
                    if (pop.dvars().trajC[j] == 1):
                        return(["C",j, hCC, hOO, hCO, R, gen_virulence])
                    # If there is a fixation of the O allele
                    if (pop.dvars().trajO[j] == 1):
                        return(["O",j, hCC, hOO, hCO, R, gen_virulence])
                    # If there is a fixation of the T allele
                    elif (pop.dvars().trajT[j] == 1):
                        return(["T",j, hCC, hOO, hCO, R])

            # If there is neither fixation nor extinction
            return(["equilibre",GENERATIONS, hCC, hOO, hCO, R, gen_virulence, pop.dvars().trajT[-1], pop.dvars().trajC[-1], pop.dvars().trajO[-1]])

        else :
            print("The number of alleles NB_ALL does not correspond to the expected values (1 or 2)")

    else :
        print("The life cycle CYCLE does not correspond to the expected value (avec_meleze or sans_meleze)")

#----------------Fonctions utilisees-----------------#

def premiere_apparition(liste):
    '''
    Function to find the firt apparition of an element other that 0 in a list
    Input:
        List to evaluate
    Sortie:
        Indice of the first element of the list different of 0
    '''
    gen = 0
    if liste[0] != 0:
        gen = 1
    elif liste == [0 for x in range(len(liste))] :
        gen = float('nan')
    else :
        while (liste[gen] == 0):
            gen+=1
    return(gen)

def fixation_virulence(listeT):
    '''
    Function to find the generation of fixation of the virulence, i.e. the generation
    of disparition of the avirulent allele
    Input:
        List of the evolution of the avirulent allele frequency through generations
    Output:
        Generation of disparition of the avirulent allele
    '''
    if (listeT[-1] == 0) :
        gen_virulence = 0
        while (listeT[gen_virulence] != 0):
            gen_virulence += 1
    else :
        gen_virulence = float('nan')
    return(gen_virulence)

def condition(CYCLE):
    '''
    Function to determine the condition to give SIMUPOP for the sexual
    reproduction, depending on the clonality rate
    Input:
        Number of generations per cycle (year): CYCLE
    Sortie :
        Expression to evaluate in SIMUPOP: True if the generation considered (gen)
        is different of 0 and a multiple of CYCLE
    '''
    return("gen != 0 and gen %% %d == 0" % CYCLE)

#------------------------------------------------------------------------

main()
