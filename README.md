Simulation model to study the evolution of population sizes and the frequency of the virulent allele through a resistance breakdown event.

Codes of the simulation model:

    Model_resistance_durability_diploid.py
    Model_resistance_durability_haploid.py

Codes for the data analyses:

    Transformation_python_results_dip.py
    Transformation_python_results_hap.py
    Statistical_Analyses_And_Graphics.Rmd
    Transformation_python_results_Restricted_design.Rmd
