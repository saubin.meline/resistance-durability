"""
/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\/
Resistance durability model, results transformation for haploid individuals

Author: Méline Saubin, 2021, INRAE Nancy. ANR Clonix2D.

Versions:
Python-3.7.1
/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/\/
"""

from pylab import *
import os.path
import os

def Tableau_Plan_exp_equilibre_hap() :
    '''
    Function to regroup all the results of a regular experimental design into 2
    data files
    Input:
        One table for every parameters combination, which regroups lists of results
        per replicate:
            List of 3 elements:
            [simulation equilibrium(fixation of "C", fixation of "T", "extinction", or "equilibre" for the coexistence between C and T),
             generation of fixation or extinction,
             generation of apparition of the first individual on R]
    Output:
        Table Fixation.txt of 24 columns:
            ["frequence", "r", "mig", "KR", "cycle", "freq_fix_C",
            "gen_fix_C", "freq_ext", "gen_ext", "freq_fix_T", "gen_fix_T",
            "freq_eq", "gen_prem_vir", "gen_prem_R",  "Q025_fix_C", "Q50_fix_C",
            "Q975_fix_C", "Q025_ext", "Q50_ext", "Q975_ext", "Q025_fix_T",
            "Q50_fix_T", "Q975_fix_T", "var_prem_vir", "var_prem_R"]
            and one row for each combination of parameters
            Number of rows = number of combinations of parameters
        Table Generation.txt of 10 columns:
            ["frequence", "r", "mig", "KR", "cycle", "gen_fix", "prem_R", "extinction", "fix_virulence", "fix_avirulence"]
            and one row for each replicate
            Number of rows = number of combinations*number of replicates per combination
            The three last columns corresponds to 0 or 1 for False or True
    '''

    nb_replicats = 100 # Number of replicates

    # Directory of the output files
    directory = "/Commun/SAUBIN/THESE/Modelisation/Python/Reprise_M2/Resultats/Plan_complet_nv_logistique_correct_mig_article_01_2021/Plan_nv_logistique_hap_propR_0-1_0-9/Tableaux/"
    # Directory folder of the input result files
    directory2 = "/Commun/SAUBIN/THESE/Modelisation/Python/Reprise_M2/Resultats/Plan_complet_nv_logistique_correct_mig_article_01_2021/Plan_nv_logistique_hap_propR_0-1_0-9/"


    # Regular experimental design used
    liste_freq = [0.0005, 0.002, 0.01, 0.05,0.15, 0.3] # favr
    liste_r = [1.1, 1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2] # r
    liste_mig = [0.05, 0.1, 0.2] # mig
    liste_KR = [10000,90000] # KR (propR = KR/100000)
    liste_cycle = ['Avec_Meleze', 'Sans_Meleze'] # Cycle:
    # "Avec_Meleze" stands for with host alternation and "Sans_Meleze" for without host alternation
    
    # Initialisation of the two empty output tables
    Tableau_parametres_fixation = []
    Tableau_parametres_generation = []

    for e in liste_cycle:
        for d in liste_KR:
            for c in liste_mig:
                for b in liste_r:
                    for a in liste_freq:
                        Tableau_parametres_fixation.append([a,b,c,d,e, 0,0, 0,0,0,0,0,'nan',0,'nan','nan','nan','nan','nan','nan','nan','nan','nan','nan','nan'])
                        for x in range(nb_replicats):
                            Tableau_parametres_generation.append([a,b,c,d,e, 'nan', 'nan', 'nan', 'nan', 'nan'])

    # Creation of the ouptut files
    Tableau_fixation = 'Fixation.txt'
    fichier_fixation = os.path.join(directory,Tableau_fixation)
    Tableau_generation = 'Generation.txt'
    fichier_generation = os.path.join(directory,Tableau_generation)

    # Scaning of all result files of the experimental design, one after the other
    with open(fichier_fixation, 'w') as fichier_fixation_modif, open(fichier_generation, 'w') as fichier_generation_modif:
        for i in range(len(liste_freq)):
            for j in range(len(liste_r)):
                for k in range(len(liste_mig)):
                    for l in range(len(liste_KR)):
                        for m in range(len(liste_cycle)):
                            # File name adapted to the output of the python script Model_resistance_durability_haploid.py, to adapt if the file names are different
                            Resultat = 'Resultat_V%d_K%d_r%d_M%d_C%d.txt' % (i+1,l+1,j+1,k+1,m+1)
                            fichier_resultat = os.path.join(directory2,Resultat)

                            # Opening of the result file
                            with open(fichier_resultat, 'r') as fichier_resultat_lecture:
                                data = []
                                for line in fichier_resultat_lecture :
                                    data.append(line)
                                # storage of the result in the vector Res
                                Res = list(eval(data[2].replace("]","],")))

                                # Initialisation of the number of coexistences
                                freq_eq = 0
                                # Initialisation of the vectors of generations of fixations and extinction
                                gen_fix = [] # Generation of virulence fixation
                                gen_ext = [] # Generation of extinction
                                gen_T = [] # Generation of avirulence fixation

                                # Initialisation of the vector of mean generation of observation of the first individual on R
                                gen_premier_R = []

                                # In Res, there is one vector per replicate
                                for x in range(nb_replicats):
                                    # Implementation of all the result vectors
                                    if np.invert(np.isnan(Res[x][2])):
                                        gen_premier_R.append(Res[x][2])
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][6] = Res[x][2]
                                    if Res[x][0] == 'C' :
                                        # Fixation of the virulent allele
                                        gen_fix.append(Res[x][1])
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][5] = Res[x][1]
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][7] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][8] = 1
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][9] = 0
                                    if Res[x][0] == 'extinction' :
                                        # Extinction of the population
                                        gen_ext.append(Res[x][1])
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][7] = 1
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][8] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][9] = 0
                                    if Res[x][0] == 'equilibre' :
                                        # Coexistence between avr and Avr
                                        freq_eq += 1
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][7] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][8] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][9] = 0
                                    if Res[x][0] == 'T' :
                                        # Fixation of the avirulent allele
                                        gen_T.append(Res[x][1])
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][7] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][8] = 0
                                        Tableau_parametres_generation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+l*len(liste_mig)*len(liste_r)*len(liste_freq)*nb_replicats+k*len(liste_r)*len(liste_freq)*nb_replicats+j*len(liste_freq)*nb_replicats+i*nb_replicats+x][9] = 1

                                # Calculations of the frequency of fixations and extinction over all replicates
                                if (len(gen_ext) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][7]= (len(gen_ext)/nb_replicats)
                                if (len(gen_fix) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][5]= (len(gen_fix)/nb_replicats)
                                if (len(gen_T) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][9]= (len(gen_T)/nb_replicats)

                                # Mean and variance of the generation of the first individual on R
                                if gen_premier_R != [] :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][13]= (mean(gen_premier_R))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][24]= (var(gen_premier_R))

                                # Frequency of coexistences over all replicates
                                if (freq_eq != 0):
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][11]= (freq_eq/nb_replicats)

                                # Mean generations of fixations and extinction
                                Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][6]= (mean(gen_fix))
                                Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][8]= (mean(gen_ext))
                                Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][10]= (mean(gen_T))

                                # Quantiles for the generations of fixations and extinction
                                if (len(gen_fix) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][14]= (np.quantile(gen_fix,0.025))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][15]= (np.quantile(gen_fix,0.5))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][16]= (np.quantile(gen_fix,0.975))
                                if (len(gen_ext) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][17]= (np.quantile(gen_ext,0.025))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][18]= (np.quantile(gen_ext,0.5))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][19]= (np.quantile(gen_ext,0.975))
                                if (len(gen_T) != 0) :
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][20]= (np.quantile(gen_T,0.025))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][21]= (np.quantile(gen_T,0.5))
                                    Tableau_parametres_fixation[m*len(liste_KR)*len(liste_mig)*len(liste_r)*len(liste_freq)+l*len(liste_mig)*len(liste_r)*len(liste_freq)+k*len(liste_r)*len(liste_freq)+j*len(liste_freq)+i][22]= (np.quantile(gen_T,0.975))
        
        # Column names for the result file Fixation.txt
        sorties_fix = ["frequence", "r", "mig", "KR", "cycle", "freq_fix_C", "gen_fix_C", "freq_ext", "gen_ext", "freq_fix_T", "gen_fix_T", "freq_eq", "gen_prem_vir", "gen_prem_R",  "Q025_fix_C", "Q50_fix_C", "Q975_fix_C", "Q025_ext", "Q50_ext", "Q975_ext", "Q025_fix_T", "Q50_fix_T", "Q975_fix_T", "var_prem_vir", "var_prem_R"]

        # Column names for the result file Generation.txt
        sorties_gen = ["frequence", "r", "mig", "KR", "cycle", "gen_fix", "prem_R", "extinction", "fix_virulence", "fix_avirulence"]

        fichier_fixation_modif.write(' '.join(sorties_fix))
        fichier_fixation_modif.write('\n')

        fichier_generation_modif.write(' '.join(sorties_gen))
        fichier_generation_modif.write('\n')

        for ligne in Tableau_parametres_fixation:
            fichier_fixation_modif.write(' '.join([str(l) for l in ligne]))
            fichier_fixation_modif.write('\n')

        for ligne in Tableau_parametres_generation:
            fichier_generation_modif.write(' '.join([str(l) for l in ligne]))
            fichier_generation_modif.write('\n')

    return()


def Tableau_Plan_exp_aleatoire_hap():
    '''
    Function to regroup all the results of a random experimental design into 2
    data files
    Input:
        One table for every parameters combination, which regroups lists of results
        per replicate:
            Lists of 3 elements:
            [simulation equilibrium(fixation of "C", fixation of "T", "extinction", or "equilibre" for the coexistence between C and T),
             generation of fixation or extinction,
             generation of apparition of the first individual on R]
    Output:
        Table Fixation.txt of 24 columns:
            ["frequence", "r", "mig", "KR", "cycle", "freq_fix_C",
            "gen_fix_C", "freq_ext", "gen_ext", "freq_fix_T", "gen_fix_T",
            "freq_eq", "gen_prem_vir", "gen_prem_R",  "Q025_fix_C", "Q50_fix_C",
            "Q975_fix_C", "Q025_ext", "Q50_ext", "Q975_ext", "Q025_fix_T",
            "Q50_fix_T", "Q975_fix_T", "var_prem_vir", "var_prem_R"]
            and one row for each combination of parameters
            Number of rows = number of combinations of parameters
        Table Generation.txt of 10 columns:
            ["frequence", "r", "mig", "KR", "cycle", "gen_fix", "prem_R", "extinction", "fix_virulence", "fix_avirulence"]
            and one row for each replicate
            Number of rows = number of combinations*number of replicates per combination
            The three last columns corresponds to 0 or 1 for False or True
    '''
    nb_replicats = 100 # Number of replicates
    
    # Initialisation of the two empty tables
    Tableau_parametres_fixation = []
    Tableau_parametres_generation = []

    # Initialisation of i and j for the indexation of the source files: Resultat_i_j.txt with i from 1 to 28
    i = 1
    j = 1
    imax = 28
    
    # Initialisation of the index combination: row indice in the final result file Fixation.txt
    combinaison = 0
    # Directory folder for the two output result files
    directory = "C:/Users/msaubin/Documents/SAUBIN_modif_confinement_10/Resultats_simulations/Plan_complet_sensi_hap/"

    # Creation of the ouptut files
    Tableau_fixation = 'Fixation.txt'
    fichier_fixation = os.path.join(directory,Tableau_fixation)
    Tableau_generation = 'Generation.txt'
    fichier_generation = os.path.join(directory,Tableau_generation)

    # Scaning of all result files of the experimental design, one after the other, by varying i and j
    while i <= imax :
        # Directory folder for all the result files of the random design.
        directory2 = "C:/Users/msaubin/Documents/SAUBIN_modif_confinement_10/Resultats_simulations/Plan_complet_sensi_hap/"

        # Name of the input file
        Resultat = 'Resultat_i%d_j%d.txt' % (i,j)

        # If the file exists, extraction of the information
        if os.path.exists(os.path.join(directory2,Resultat)):
            # New combination of parameters
            combinaison += 1
            print(i,j,combinaison)
            fichier_resultat = os.path.join(directory2,Resultat)

            with open(fichier_resultat, 'r') as fichier_resultat_lecture:
                # Initialisation of the vector data to store all the rows of the file
                data = []
                for line in fichier_resultat_lecture :
                    data.append(line)

                # Extraction of the parameters for this result file
                Parametres = list(eval(data[1].replace("avec_meleze","\"avec_meleze\"").replace("sans_meleze","\"sans_meleze\"").replace(" ",", ")))
                frequence = Parametres[0]
                r = Parametres[3]
                mig = Parametres[4]
                KR = Parametres[2]
                cycle = Parametres[5]

                # Initialisation of the rows summarizing the results for the Fixation and the Generation table
                Tableau_parametres_fixation.append([frequence,r,mig,KR,cycle, 0,0, 0,0,0,0,0,0,0,'nan','nan','nan','nan','nan','nan','nan','nan','nan','nan','nan'])
                for w in range(nb_replicats):
                    Tableau_parametres_generation.append([frequence,r,mig,KR,cycle, 'nan', 'nan', 'nan', 'nan', 'nan'])

                Res = list(eval(data[2].replace("]","],")))

                # Initialisation of the number of coexistences 
                freq_eq = 0
                # Initialisation of the vectors of generations of fixations and extinction
                gen_fix = [] # Generation of virulence fixation
                gen_ext = [] # Generation of extinction
                gen_T = []   # Generation of avirulence fixation

                # Initialisation of the vector of mean generation of observation of the first individual on R
                gen_premier_R = []

                # In Res, there is one vector per replicate
                for x in range(nb_replicats):
                    # Implementation of all the result vectors
                    if np.invert(np.isnan(Res[x][2])):
                        gen_premier_R.append(Res[x][2])
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][6] = Res[x][2]
                    if Res[x][0] == 'C' :
                        # Fixation of the virulent allele
                        gen_fix.append(Res[x][1])
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][5] = Res[x][1]
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][7] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][8] = 1
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][9] = 0
                    if Res[x][0] == 'extinction' :
                        # Extinction of the population
                        gen_ext.append(Res[x][1])
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][7] = 1
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][8] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][9] = 0
                    if Res[x][0] == 'equilibre' :
                        # Coexistence between avr (C) and Avr (T)
                        freq_eq += 1
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][7] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][8] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][9] = 0
                    if Res[x][0] == 'T' :
                        # Fixation af the avirulent allele
                        gen_T.append(Res[x][1])
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][7] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][8] = 0
                        Tableau_parametres_generation[(combinaison-1)*nb_replicats+x][9] = 1

                # Calculations of the frequency of fixations and extinction over all replicates
                if (len(gen_ext) != 0) :
                    Tableau_parametres_fixation[combinaison-1][7]= (len(gen_ext)/nb_replicats)
                if (len(gen_fix) != 0) :
                    Tableau_parametres_fixation[combinaison-1][5]= (len(gen_fix)/nb_replicats)
                if (len(gen_T) != 0) :
                    Tableau_parametres_fixation[combinaison-1][9]= (len(gen_T)/nb_replicats)

                # Mean and variance of the generation of the first individual on R
                if gen_premier_R != [] :
                    Tableau_parametres_fixation[combinaison-1][13]= (mean(gen_premier_R))
                    Tableau_parametres_fixation[combinaison-1][24]= (var(gen_premier_R))

                # Frequency of coexistences over all replicates
                if (freq_eq != 0):
                    Tableau_parametres_fixation[combinaison-1][11]= (freq_eq/nb_replicats)

                # Mean generations of fixations and extinction
                Tableau_parametres_fixation[combinaison-1][6]= (mean(gen_fix))
                Tableau_parametres_fixation[combinaison-1][8]= (mean(gen_ext))
                Tableau_parametres_fixation[combinaison-1][10]= (mean(gen_T))

                # Quantiles for the generations of fixations and extinction
                if (len(gen_fix) != 0) :
                    Tableau_parametres_fixation[combinaison-1][14]= (np.quantile(gen_fix,0.025))
                    Tableau_parametres_fixation[combinaison-1][15]= (np.quantile(gen_fix,0.5))
                    Tableau_parametres_fixation[combinaison-1][16]= (np.quantile(gen_fix,0.975))
                if (len(gen_ext) != 0) :
                    Tableau_parametres_fixation[combinaison-1][17]= (np.quantile(gen_ext,0.025))
                    Tableau_parametres_fixation[combinaison-1][18]= (np.quantile(gen_ext,0.5))
                    Tableau_parametres_fixation[combinaison-1][19]= (np.quantile(gen_ext,0.975))
                if (len(gen_T) != 0) :
                    Tableau_parametres_fixation[combinaison-1][20]= (np.quantile(gen_T,0.025))
                    Tableau_parametres_fixation[combinaison-1][21]= (np.quantile(gen_T,0.5))
                    Tableau_parametres_fixation[combinaison-1][22]= (np.quantile(gen_T,0.975))

            j += 1

        # If the file does not exist, jmax is reached for this value of i: incrementation of i
        else :
            i += 1
            j = 1

    with open(fichier_fixation, 'w') as fichier_fixation_modif, open(fichier_generation, 'w') as fichier_generation_modif:

        # Column names for the result file Fixation.txt
        sorties_fix = ["frequence", "r", "mig", "KR", "cycle", "freq_fix_C", "gen_fix_C", "freq_ext", "gen_ext", "freq_fix_T", "gen_fix_T", "freq_eq", "gen_prem_vir", "gen_prem_R",  "Q025_fix_C", "Q50_fix_C", "Q975_fix_C", "Q025_ext", "Q50_ext", "Q975_ext", "Q025_fix_T", "Q50_fix_T", "Q975_fix_T", "var_prem_vir", "var_prem_R"]

        # Column names for the result file Generation.txt
        sorties_gen = ["frequence", "r", "mig", "KR", "cycle", "gen_fix", "prem_R", "extinction", "fix_virulence", "fix_avirulence"]

        fichier_fixation_modif.write(' '.join(sorties_fix))
        fichier_fixation_modif.write('\n')

        fichier_generation_modif.write(' '.join(sorties_gen))
        fichier_generation_modif.write('\n')

        for ligne in Tableau_parametres_fixation:
            fichier_fixation_modif.write(' '.join([str(l) for l in ligne]))
            fichier_fixation_modif.write('\n')

        for ligne in Tableau_parametres_generation:
            fichier_generation_modif.write(' '.join([str(l) for l in ligne]))
            fichier_generation_modif.write('\n')

    return()

if __name__ == "__main__":
    Tableau_Plan_exp_equilibre_hap()
